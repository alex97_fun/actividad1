
import {Label} from '../milib/views/labels/label';
import {Button,ButtonListener} from '../milib/views/buttons/button';
import {DataHolder} from '../milib/dataholder/dataholder';
import {Motor} from '../milib/engines/motor';
import {Imagen} from '../milib/views/imgs/imagen';
import {Panel} from '../milib/views/panels/panel';

export class Actividad1 implements ButtonListener{

    private motor:Motor;
    private imagenFondo:Imagen;
    private btnComenzar:Button;
    private btnReanudar:Button;
    private btnSalir:Button;
    private imagenQuiz:Imagen;
    private imagenTrans:Imagen;
    private panel1:Panel;
    private wGanar:Imagen;
    private wPerder:Imagen;
    private lblPregunta:Label;
    private imgLblPregunta:Label;
    private btn1:Button;
    private btn2:Button;
    private btn3:Button;
    private btn4:Button;

    private arrPr:Array<string>;
    private arrRes:Array<string[]>;
    private arrResOk:Array<number>;
    private indice:number;


    constructor(vMotor:Motor){
        this.motor=vMotor;
        this.imagenFondo=new Imagen(this.motor,0,0,DataHolder.instance.nScreenWidth,DataHolder.instance.nScreenHeight);
        this.imagenFondo.setImg('./assets/backmain.jpg');
        this.motor.setRaiz(this.imagenFondo);
        
        this.juegoWindow();
        this.menuWindow();
    }

    //Metodo para pintar las preguntas y mostrar la pantalla ganar
    private setTextPrRes():void{
        if(this.indice>=this.arrPr.length){
            this.ganarWindow();
        }else{
            this.lblPregunta.setTexto(this.arrPr[this.indice]);
            
            for(var f = 0; f < this.arrRes[this.indice].length; f++){
                if (f==0) {
                    this.btn1.setTexto(this.arrRes[this.indice][f]); 
                }else if (f==1) {
                    this.btn2.setTexto(this.arrRes[this.indice][f]);
                } else if(f==2){
                    this.btn3.setTexto(this.arrRes[this.indice][f]);
                }else if(f==3){
                    this.btn4.setTexto(this.arrRes[this.indice][f]);
                }
            }  
        }
    }

    //Imagen que sale cuando ganas
    private ganarWindow():void{
        this.wGanar = new Imagen(this.motor,0,0,DataHolder.instance.nScreenWidth,DataHolder.instance.nScreenHeight);
        this.wGanar.setImg('./assets/ganaste.jpg');
        this.motor.addViewToParentView(this.panel1,this.wGanar);
        this.motor.setViewVisibility(this.imagenTrans.uid,true);
        this.motor.setViewVisibility(this.wGanar.uid,true);
        
    }

    //Imagen que sale cuando pierdes
    private perderWindow():void{
        this.wPerder = new Imagen(this.motor,0,0,DataHolder.instance.nScreenWidth,DataHolder.instance.nScreenHeight);
        this.wPerder.setImg('./assets/perdiste.jpg');
        this.motor.addViewToParentView(this.panel1,this.wPerder);
        this.motor.setViewVisibility(this.imagenTrans.uid,true);
        this.motor.setViewVisibility(this.wPerder.uid,true);

    }

    private menuWindow():void{

        this.imagenQuiz=new Imagen(this.motor,0,0,DataHolder.instance.nScreenWidth>>1,DataHolder.instance.nScreenHeight);
        this.motor.addViewToParentView(this.imagenFondo,this.imagenQuiz);

        //Creando e inicializando el boton comenzar
        this.btnComenzar = new Button(this.motor,this.imagenFondo.w*0.7,this.imagenFondo.w*0.12,this.imagenQuiz.w/4,this.imagenQuiz.w*0.15);
        this.btnComenzar.setTexto("Comenzar");
        this.btnComenzar.setImagePath('./assets/boton.png');
        this.motor.addViewToParentView(this.imagenQuiz,this.btnComenzar);
        this.btnComenzar.setListener(this);

        //Creando e inicializando el boton reanudar
        this.btnReanudar = new Button(this.motor,this.imagenFondo.w*0.7,this.imagenFondo.w*0.22,this.imagenQuiz.w/4,this.imagenQuiz.w*0.15);
        this.btnReanudar.setTexto("Reanudar");
        this.btnReanudar.setImagePath('./assets/boton.png');
        this.motor.addViewToParentView(this.imagenQuiz,this.btnReanudar);
        this.btnReanudar.setListener(this);

        //Creando e inicializando el boton salir
        this.btnSalir = new Button(this.motor,this.imagenFondo.w*0.7,this.imagenFondo.w*0.32,this.imagenQuiz.w/4,this.imagenQuiz.w*0.15);
        this.btnSalir.setTexto("Salir");
        this.btnSalir.setImagePath('./assets/boton.png');
        this.motor.addViewToParentView(this.imagenQuiz,this.btnSalir);
        this.btnSalir.setListener(this);
         
    }

    private juegoWindow():void{

        //Array de las preguntas
        this.arrPr = [
            "12 + 31 - 7 = ?",
            "10 x 3 = ?",
            "273 - 321 = ?",
            "2000 / 50 = ?",
            "2^8 = ?",
            "(6/3) x (12/9) = ?",
            "5 + 7 x 3 = ?"
        ];

        //Array de las respuestas
        this.arrRes = [
            ["36", "33", "38", "39"],
            ["13", "33", "7","30"],
            ["-43", "-58", "-48", "53"],
            ["40", "80", "20", "200"],
            ["128", "256", "512", "1024"],
            ["(8/3)", "(2/3)", "(10/3)", "1"],
            ["32", "36", "26", "40"],

        ];

        //Array de las respuestas correctas
        this.arrResOk = [
            0,
            3,
            2,
            0,
            1,
            0,
            2
        ];
        this.indice=0;
        
        //Quiz Panel
        this.panel1= new Panel(this.motor,0,0,DataHolder.instance.nScreenWidth,DataHolder.instance.nScreenHeight);
        this.motor.addViewToParentView(this.imagenFondo,this.panel1);
        this.panel1.btnSalir.setListener(this);

        //Imagen sobre ventana para utilizar el window para ganar y perder
        this.imagenTrans =new Imagen(this.motor,0,0,DataHolder.instance.nScreenWidth>>1,DataHolder.instance.nScreenHeight);
        this.motor.addViewToParentView(this.panel1,this.imagenTrans);

        //Pregunta
        this.imgLblPregunta = new Label (this.motor,DataHolder.instance.nScreenWidth*0.635,DataHolder.instance.nScreenHeight*0.2,DataHolder.instance.nScreenWidth>>2,DataHolder.instance.nScreenHeight>>2.3);
        this.imgLblPregunta.setImagePath('./assets/pizarra.png');
        this.motor.addViewToParentView(this.imagenTrans,this.imgLblPregunta);

        this.lblPregunta = new Label (this.motor,DataHolder.instance.nScreenWidth*0.635,DataHolder.instance.nScreenHeight*0.2,DataHolder.instance.nScreenWidth>>2,DataHolder.instance.nScreenHeight>>2.3);
        this.lblPregunta.setTexto("Pregunta");
        this.motor.addViewToParentView(this.imagenTrans,this.lblPregunta);

        //Respuesta1
        this.btn1 = new Button(this.motor,DataHolder.instance.nScreenWidth*0.6,DataHolder.instance.nScreenHeight*0.58,DataHolder.instance.nScreenWidth>>3.7,DataHolder.instance.nScreenHeight>>3.85);
        this.btn1.setTexto("Respuesta 1");
        this.btn1.setImagePath('./assets/boton.png');
        this.motor.addViewToParentView(this.imagenTrans,this.btn1);
        this.btn1.setListener(this);

        //Respuesta2
        this.btn2 = new Button(this.motor,DataHolder.instance.nScreenWidth*0.6,DataHolder.instance.nScreenHeight*0.76,DataHolder.instance.nScreenWidth>>3.7,DataHolder.instance.nScreenHeight>>3.85);
        this.btn2.setTexto("Respuesta 2");
        this.btn2.setImagePath('./assets/boton.png');
        this.motor.addViewToParentView(this.imagenTrans,this.btn2);
        this.btn2.setListener(this);

        //Respuesta3
        this.btn3 = new Button(this.motor,DataHolder.instance.nScreenWidth*0.8,DataHolder.instance.nScreenHeight*0.58,DataHolder.instance.nScreenWidth>>3.7,DataHolder.instance.nScreenHeight>>3.85);
        this.btn3.setTexto("Respuesta 3");
        this.btn3.setImagePath('./assets/boton.png');
        this.motor.addViewToParentView(this.imagenTrans,this.btn3);
        this.btn3.setListener(this);

        //Respuesta4
        this.btn4 = new Button(this.motor,DataHolder.instance.nScreenWidth*0.8,DataHolder.instance.nScreenHeight*0.76,DataHolder.instance.nScreenWidth>>3.7,DataHolder.instance.nScreenHeight>>3.85);
        this.btn4.setTexto("Respuesta 4");
        this.btn4.setImagePath('./assets/boton.png');
        this.motor.addViewToParentView(this.imagenTrans,this.btn4);
        this.btn4.setListener(this);

    }

    buttonListenerOnClick?(btn:Button):void{
        if (this.btnComenzar==btn) {
            this.indice=0;
            this.motor.setViewVisibility(this.imagenQuiz.uid,false);
            this.motor.setViewVisibility(this.panel1.uid,true);
            this.setTextPrRes();
            
        }else if(this.btnReanudar==btn){
            this.motor.setViewVisibility(this.imagenQuiz.uid,false);
            this.motor.setViewVisibility(this.panel1.uid,true);
            this.motor.setViewVisibility(this.imagenTrans.uid,true);
            this.setTextPrRes();

        }else if (this.panel1.btnSalir==btn) {
            this.motor.setViewVisibility(this.imagenQuiz.uid,true);
            this.motor.setViewVisibility(this.panel1.uid,false);
            this.motor.setViewVisibility(this.imagenTrans.uid,false);
            

        }else if (this.btnSalir==btn) {
            this.motor.setViewVisibility(this.imagenQuiz.uid,false);
            this.motor.setViewVisibility(this.panel1.uid,false);
            this.motor.setViewVisibility(this.imagenTrans.uid,false);
            

        }else if (this.btn1==btn) {
            if (this.btn1.texto === this.arrRes[this.indice][this.arrResOk[this.indice]]) {
                this.indice=this.indice+1;
                this.setTextPrRes();
                
            }else{
                this.perderWindow();
            
            }
            
        }else if (this.btn2==btn) {
            if (this.btn2.texto === this.arrRes[this.indice][this.arrResOk[this.indice]]) {
                this.indice=this.indice+1;
                this.setTextPrRes();
               
            }else{
                this.perderWindow();
                
            }
          
        }else if (this.btn3==btn) {
            if (this.btn3.texto === this.arrRes[this.indice][this.arrResOk[this.indice]]) {
                this.indice=this.indice+1;
                this.setTextPrRes();
              
            }else{
                this.perderWindow();
                
            }

        }else if (this.btn4==btn) {
            if (this.btn4.texto === this.arrRes[this.indice][this.arrResOk[this.indice]]) {
                this.indice=this.indice+1;
                this.setTextPrRes();
               
            }else{
                this.perderWindow();
            }
            
        }  
          
    }
      
}